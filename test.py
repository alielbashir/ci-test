import unittest
from subprocess import Popen, PIPE

class tests(unittest.TestCase):

    def test_1(self):
        server = Popen("./server", stdout=PIPE)
        client = Popen("./client", stdout=PIPE)

        expectedServerOutput = b'Client : Hello from client\nHello message sent.\n'
        realServerOutput = server.stdout.read()
        self.assertEqual(expectedServerOutput, realServerOutput)
    
    def test_2(self):
        server = Popen("./server", stdout=PIPE)
        client = Popen("./client", stdout=PIPE)

        expectedServerOutput = b'Client : Hello from client\nHello message sent.\n'
        realServerOutput = server.stdout.read()
        self.assertEqual(expectedServerOutput, realServerOutput)


unittest.main()
